# clementeyene

ClementEYEne, simple bash script that gets Clementine Music Player status over dbus

# Installation

Arch Linux users can simply install from the aur:
```bash
paru -S clementeyene
```

Non-arch users can just `git clone` this repository and copy the `clementeyene` file to their /usr/bin.


Known Issues:

Does not work with icecast radio

Any `dbus-monitor` instances started 1 second or less before the program is launched with any flags will be force termed. This is because of a bodge I had to do to kill all of the `dbus-monitor` child processes clementeyene uses to listen for signals from Clementine.
